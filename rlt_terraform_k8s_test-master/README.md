# rlt_terraform_k8s_test
This repo holds the assets needed for our Terraform, Kubernetes, And Helm coding test

## Test Overview
The purpose of this test is to demonstrate your knowledge in the following areas:
* GCP
* Terraform
* Kubernetes (GKE)
* Helm

This repo holds the application code and Dockerfile in the "application" directory. The helm chart to be used to deploy the application to the Kubernetes cluster is the "charts" directory.

## Test Instructions
1) Create Terraform code to deploy a Kubernetes cluster inside of GCP.
2) Build rlt-test application image and push GCR
3) Deploy the helm chart included in the repo into the kubernetes cluster.  
4) Fix any issues that may be present in the helm chart.
5) Expose the application to the outside world.  
