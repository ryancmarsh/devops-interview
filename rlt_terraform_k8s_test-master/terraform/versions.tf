
terraform {
  required_version = ">= 0.13"
}

provider "google" {
  credentials = "/Users/ryanmarsh/Downloads/ceds.json"
  project = var.project_id
  region  = var.region
}
